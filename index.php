<?php
function valid_input($input) {
    if ((!$input['offer_id'] || !is_numeric($input['offer_id']))
        && (!$input['affiliate_id'] || !is_numeric($input['affiliate_id']))
        && (!$input['campaign_id'] || !is_numeric($input['campaign_id']))) {
            return false;
    }
    return true;
}

if ($_POST['form_submitted'] == true) {
    if (!valid_input($_POST)) {
        $api_response['success'] = false;
        $api_response['message'] = 'You must enter at least one numeric ID.';
    } else {
        foreach ($_POST as $key => $value) {
            if ($key == 'form_submitted') {
                continue;
            }
            if ($key == 'offer_id' || $key == 'affiliate_id' || $key == 'campaign_id') {
                if (!$value) $value = 0;
            }        
            $post_data[] = $key.'='.$value;
        }
        $query_str = implode('&', $post_data);
        include_once('find-capped-campaigns.php');
        $api_response = json_decode(get_cake_results($query_str), true);
    }
} else {
    $api_response = '';
}
?>

<html><head><title>Find Cake Campaigns with Caps</title></head>
<body>
<h1>Hi Aaron!</h1>
<p>Fill in at least one of the IDs to find out if there are any caps.</p>
<form method="post" action="/cake-api-tool/">
    Offer ID: <input type="text" name="offer_id" value="<?php echo($_POST['offer_id']);?>"><br />
    Affiliate ID: <input type="text" name="affiliate_id" value="<?php echo($_POST['affiliate_id']);?>"><br />
    Campaign ID: <input type="text" name="campaign_id" value="<?php echo($_POST['campaign_id']); ?>"><br />
    <input type="hidden" name="form_submitted" value="1" />
    <input type="submit" value="Submit">
</form>
<?php
    if ($api_response) {
        echo('<hr />');
        if ($api_response['success']) {
            if (isset($api_response['summary'])) {
                echo('<p>'.$api_response['summary'].'</p>');
            }
            echo('<table border="1"><tr>');
            foreach($api_response['results']['keys'] as $column) {
                echo('<th>'.ucfirst($column).'</th>');
            }
            echo('</tr>');
            unset($api_response['results']['keys']);
            foreach($api_response['results'] as $row) {
                echo('<tr>');
                foreach($row as $value) {
                    echo('<td>'.$value.'</td>');
                }
                echo('</tr>');
            }
            echo('</table>');
        } else {
          echo('<p style="color:red">Error: '.$api_response['message'].'</p>');  
        }
    }
?>
</body>
</html>
