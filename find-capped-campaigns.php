<?php 

function get_cake_results($query_str) {
    $response = array();
    $api_key = 'oEGjdmzSCH7omaCj7IcmUm1NC9LTFp';
    $query_str .= '&api_key='.$api_key;
    $query_str .= '&account_status_id=0&media_type_id=0&start_at_row=0&row_limit=0&sort_field=0&sort_descending=TRUE';
    $url = 'http://network.neverblue.com/api/6/export.asmx/Campaigns?';
    $xml = post_url($url.$query_str);    
    
    if (strtolower($xml['CAMPAIGN_EXPORT_RESPONSE']['SUCCESS']) == 'false') {
        $response['success'] = false;
        $response['message'] = $xml['CAMPAIGN_EXPORT_RESPONSE']['MESSAGE'];
        return json_encode($response);
    } else {
        return print_results($xml);
    }
}

function post_url($url) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $response = curl_exec($ch);
    curl_close($ch);
    
    $xml = XMLtoArray($response);
    return $xml;
}

function print_results($xml) {
    $response = array();    
    $total_campaigns = 0;
    $capped_campaigns = 0;
    if (isset($xml['CAMPAIGN_EXPORT_RESPONSE']['CAMPAIGNS'])) {
        $response['results'] = array();
        $shift_array = (($xml['CAMPAIGN_EXPORT_RESPONSE']['ROW_COUNT'] === '1')? 0:1);

        foreach ($xml['CAMPAIGN_EXPORT_RESPONSE']['CAMPAIGNS'] as $campaign) {             
            if (!$shift_array) {
                $temp = array();
                $total_campaigns++;
                $offer = $campaign['OFFER'];
                $affiliate = $campaign['AFFILIATE']['AFFILIATE_ID']['content'];
                $aff_campaign = $campaign['CAMPAIGN_ID'];
                $click_cap = $campaign['CLICK_CAP'];
                $click_cap_interval = $click_cap['CAP_INTERVAL']['CAP_INTERVAL_NAME']['content'];
                if (is_array($click_cap['LIMIT'])) {
                    $click_cap_limit = '-';
                } else {
                    $click_cap_limit = 'Interval: '.$click_cap_interval.'<br />Limit: '.$click_cap['LIMIT'];
                }
                $conversion_cap = $campaign['CONVERSION_CAP'];
                $conversion_cap_interval = $conversion_cap['CAP_INTERVAL']['CAP_INTERVAL_NAME']['content'];    
                if (is_array($conversion_cap['LIMIT'])) {
                    $conversion_cap_limit = '-';
                } else {
                    $conversion_cap_limit = 'Interval: '.$conversion_cap_interval.'<br />Limit: '.$conversion_cap['LIMIT'];
                }
                
                $temp = array($affiliate, $offer['OFFER_ID']['content'], $offer['OFFER_NAME']['content'],
                             $aff_campaign, $click_cap_limit, $conversion_cap_limit);
                
                if (!is_array($click_cap['LIMIT']) || !is_array($conversion_cap['LIMIT'])) {
                    // only add to results if there is a limit set
                    $response['results'][] = $temp;
                    $capped_campaigns++;
                }

            } else {
                foreach($campaign as $thiscampaign) {
                    $total_campaigns++;
                    $offer = array_shift($thiscampaign['OFFER']);
                    $affiliate = array_shift($thiscampaign['AFFILIATE']);
                    $aff_campaign = $thiscampaign['CAMPAIGN_ID'];
                    $click_cap = array_shift($thiscampaign['CLICK_CAP']);                    
                    $click_cap_interval = array_shift($click_cap['CAP_INTERVAL']);
                    if (is_array($click_cap['LIMIT'])) {
                        $click_cap_limit = '-';
                    } else {
                        $click_cap_limit = 'Interval: '.$click_cap_interval['CAP_INTERVAL_NAME']['content'].'<br />Limit: '.$click_cap['LIMIT'];
                    }
                    $conversion_cap = array_shift($thiscampaign['CONVERSION_CAP']);                    
                    $conversion_cap_interval = array_shift($conversion_cap['CAP_INTERVAL']);
                    if (is_array($conversion_cap['LIMIT'])) {
                        $conversion_cap_limit = '-';
                    } else {
                        $conversion_cap_limit = 'Interval: '.$conversion_cap_interval['CAP_INTERVAL_NAME']['content'].'<br />Limit: '.$conversion_cap['LIMIT'];
                    }
                    $count++;
                    
                    $temp = array($affiliate['AFFILIATE_ID']['content'], $offer['OFFER_ID']['content'], $offer['OFFER_NAME']['content'],
                                 $aff_campaign, $click_cap_limit, $conversion_cap_limit);

                    if (!is_array($click_cap['LIMIT']) || !is_array($conversion_cap['LIMIT'])) {    
                        $response['results'][] = $temp;
                        $capped_campaigns++;
                    }
                }
            }
        }      

        if (sizeof($response['results']) > 0) {
            $response['results']['keys'] = array('affiliate', 'offer_id', 'offer_name', 'campaign', 'click_cap', 'conversion_cap');
            $response['success'] = true;
        } else {
            $response['success'] = false;
            $response['message'] = 'No campaigns found with caps.';
        }        
    } else {
        if (!$xml['ROW_COUNT']) {
            $response['success'] = false;
            $response['message'] = 'Could not find any campaigns.';
        } else {
            $xml_output = print_r($xml, true);
            $response['success'] = false;
            $response['message'] = 'Error: Unknown error. '.$xml_output;
        }
        
    }
    $response['summary'] = $capped_campaigns.' out of '.$total_campaigns.' campaigns have caps.';
    return json_encode($response);
}

function XMLtoArray($XML) {
    $xml_parser = xml_parser_create();
    xml_parse_into_struct($xml_parser, $XML, $vals);
    xml_parser_free($xml_parser);
    $_tmp='';
    foreach ($vals as $xml_elem) {
        $x_tag=$xml_elem['tag'];
        $x_level=$xml_elem['level'];
        $x_type=$xml_elem['type'];
        if ($x_level!=1 && $x_type == 'close') {
            if (isset($multi_key[$x_tag][$x_level]))
                $multi_key[$x_tag][$x_level]=1;
            else
                $multi_key[$x_tag][$x_level]=0;
        }
        if ($x_level!=1 && $x_type == 'complete') {
            if ($_tmp==$x_tag)
                $multi_key[$x_tag][$x_level]=1;
            $_tmp=$x_tag;
        }
    }
    // jedziemy po tablicy
    foreach ($vals as $xml_elem) {
        $x_tag=$xml_elem['tag'];
        $x_level=$xml_elem['level'];
        $x_type=$xml_elem['type'];
        if ($x_type == 'open')
            $level[$x_level] = $x_tag;
        $start_level = 1;
        $php_stmt = '$xml_array';
        if ($x_type=='close' && $x_level!=1)
            $multi_key[$x_tag][$x_level]++;
        while ($start_level < $x_level) {
            $php_stmt .= '[$level['.$start_level.']]';
            if (isset($multi_key[$level[$start_level]][$start_level]) && $multi_key[$level[$start_level]][$start_level])
                $php_stmt .= '['.($multi_key[$level[$start_level]][$start_level]-1).']';
            $start_level++;
        }
        $add='';
        if (isset($multi_key[$x_tag][$x_level]) && $multi_key[$x_tag][$x_level] && ($x_type=='open' || $x_type=='complete')) {
            if (!isset($multi_key2[$x_tag][$x_level]))
                $multi_key2[$x_tag][$x_level]=0;
            else
                $multi_key2[$x_tag][$x_level]++;
            $add='['.$multi_key2[$x_tag][$x_level].']';
        }
        if (isset($xml_elem['value']) && trim($xml_elem['value'])!='' && !array_key_exists('attributes', $xml_elem)) {
            if ($x_type == 'open')
                $php_stmt_main=$php_stmt.'[$x_type]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
            else
                $php_stmt_main=$php_stmt.'[$x_tag]'.$add.' = $xml_elem[\'value\'];';
            eval($php_stmt_main);
        }
        if (array_key_exists('attributes', $xml_elem)) {
            if (isset($xml_elem['value'])) {
                $php_stmt_main=$php_stmt.'[$x_tag]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
                eval($php_stmt_main);
            }
            foreach ($xml_elem['attributes'] as $key=>$value) {
                $php_stmt_att=$php_stmt.'[$x_tag]'.$add.'[$key] = $value;';
                eval($php_stmt_att);
            }
        }
    }
    return $xml_array;
}

?>
